$(document).ready(function() {
    $('.btn-login-open').on('click', function() {
        $('.modal-box.login-box').show();
    });

    $('.modal-box').on('click', function() {
        $('.modal-box').hide();
    });

    $('.modal-box .modal-wrapper').on('click', function(event) {
        event.stopPropagation();
    });

    $('.modal-box .btn-close').on('click', function(event) {
        $('.modal-box').hide();
    });

    var send = false;
    $('.modal-box.login-box .login-wrapper button.login').on('click', function() {
        if(send) {
            alert("로그인 중 입니다.");
            return;
        }
        send = true;
        var $this = $(this);

        var $id = $('.modal-box.login-box .login-wrapper input[name=id]');
        var $password = $('.modal-box.login-box .login-wrapper input[name=password]');

        if($id.val() === "") {
            alert("아이디를 확인해주세요.");
            $id.focus();
            send = false;
            return;
        }

        if($password.val() === "") {
            alert("비밀번호를 확인해주세요.");
            $password.focus();
            send = false;
            return;
        }

        $.ajax({
            type: 'post',
            url: '/users/login',
            data: {
              id: $id.val(),
              password: $password.val()
            },
            success: function(result) {
                if(result.code == 0) {
                    alert("입력정보를 확인해주세요.");
                } else {
                    location.reload(true);
                }
            },
            error: function() {
                alert("잠시 후 다시 시도해주세요.");
            },
            beforeSend: function() {
                $this.addClass("loading");
            },
            complete: function() {
                $this.removeClass("loading");
                send = false;
            }
        });
    });
});