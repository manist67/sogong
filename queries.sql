CREATE TABLE `product` (
  `seq` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '',
  `contents` text NOT NULL,
  `is_sell` tinyint(1) NOT NULL DEFAULT '0',
  `is_rent` tinyint(1) NOT NULL DEFAULT '0',
  `sell_price` int(11) DEFAULT NULL,
  `rent_price` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `category` varchar(15) NOT NULL DEFAULT '',
  `author` varchar(50) NOT NULL DEFAULT '',
  `one_line` varchar(100) DEFAULT '',
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

CREATE TABLE `product_image` (
  `seq` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product` int(11) NOT NULL,
  `image_url` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_main` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;


CREATE TABLE `rent` (
  `seq` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `start_date` timestamp NOT NULL,
  `rent_day` int(11) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_cancel` tinyint(11) NOT NULL DEFAULT '0',
  `cancel_date` int(11) DEFAULT NULL,
  `etc` text NOT NULL,
  `address` text NOT NULL,
  `purpose` varchar(50) NOT NULL DEFAULT '',
  `md_checked` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


CREATE TABLE `sell` (
  `seq` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `organization` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `etc` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifted` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `md_checked` tinyint(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


CREATE TABLE `user` (
  `seq` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id` varchar(50) NOT NULL DEFAULT '',
  `password` text NOT NULL,
  `email` varchar(200) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;


CREATE TABLE `user_reset` (
  `seq` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `hash` text NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;