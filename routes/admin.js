var express = require('express');
var router = express.Router();
var sqlmap = require('../db/sqlmap');

var multer = require('multer');
var path = require("path");

const storage = multer.diskStorage({
    destination: function(req, file ,callback){
        callback(null, "upload/")
    },
    filename: function(req, file, callback){
        let extension = path.extname(file.originalname);
        callback(null, Date.now() + extension);
    }
});

const upload = multer({
    storage: storage
});

function authentication(req, res, next) {
    if(!req.session.user) {
        res.redirect("/admin");
        return;
    }
    if(req.session.user.is_admin) {
        next();
    } else {
        res.redirect("/admin");
    }
}

router.get("/", function(req, res) {
    res.render("admin/index", {});
});


router.get("/users", authentication, function(req, res) {
    const page = req.query.page ? parseInt(req.query.page): 0;

    database.connect(function(err) {
        if(err) throw err;

        database.query(sqlmap.userCount,[], function(err, result) {
            if(err) throw err;
            const count = result[0].count;

            database.query(sqlmap.userList,[page * 30], function(err, result) {
                if(err) throw err;

                res.render("admin/users", {'userList': result, "count": count, "page": page});
            });
        });
    });
});

router.get("/users/:seq", authentication, function(req, res, next) {
    const seq = parseInt(req.params.seq);

    database.connect(function(err) {
        if(err) throw err;

        database.query(sqlmap.userRetrieve,[seq], function(err, result) {
            if(err) throw err;
            if(!result.length) next();

            const user = result[0];

            database.query(sqlmap.rentRetrieveByUser, [seq], function(err, result) {
                if(err) throw err;
                const rentList = result;
                database.query(sqlmap.sellRetrieveByUser, [seq], function(err, result) {
                    const sellList = result;
                    if(err) throw err;
                    res.render("admin/user-detail", {
                        'user': user,
                        'rentList': rentList,
                        'sellList': sellList
                    });
                });
            });
        });
    });
});

router.get("/rent", authentication, function(req, res) {
    const page = req.query.page ? parseInt(req.query.page): 0;

    database.connect(function(err) {
        if(err) throw err;

        database.query(sqlmap.rentCount,[], function(err, result) {
            if(err) throw err;
            const count = result[0].count;

            database.query(sqlmap.rentList,[page * 30], function(err, result) {
                if(err) throw err;

                res.render("admin/rent", {'rentList': result, "count": count, "page": page});
            });
        });
    });
});

router.get("/rent/:seq", authentication, function(req, res) {
    const seq = parseInt(req.params.seq);

    database.connect(function(err) {
        if(err) throw err;

        database.query(sqlmap.rentRetrieve,[seq], function(err, result) {
            if(err) throw err;

            res.render("admin/rent-detail", {'rent': result[0]});
        });
    });
});

router.post("/rent/checked/:seq", authentication, function(req, res) {
    const seq = parseInt(req.params.seq);

    database.connect(function(err) {
        if(err) throw err;

        database.query(sqlmap.rentCheck,[seq], function(err, result) {
            if(err) throw err;

            res.send({"code": 1, "reason": "정상 처리되었습니다."});
        });
    });
});


router.get("/sell", authentication, function(req, res) {
    const page = req.query.page ? parseInt(req.query.page): 0;

    database.connect(function(err) {
        if(err) throw err;

        database.query(sqlmap.sellCount,[], function(err, result) {
            if(err) throw err;
            const count = result[0].count;

            database.query(sqlmap.sellList,[page * 30], function(err, result) {
                if(err) throw err;

                res.render("admin/sell", {'sellList': result, "count": count, "page": page});
            });
        });
    });
});

router.get("/sell/:seq", authentication, function(req, res) {
    const seq = parseInt(req.params.seq);

    database.connect(function(err) {
        if(err) throw err;

        database.query(sqlmap.sellRetrieve,[seq], function(err, result) {
            if(err) throw err;

            res.render("admin/sell-detail", {'sell': result[0]});
        });
    });
});

router.post("/sell/checked/:seq", authentication, function(req, res) {
    const seq = parseInt(req.params.seq);

    database.connect(function(err) {
        if(err) throw err;

        database.query(sqlmap.sellCheck,[seq], function(err, result) {
            if(err) throw err;

            res.send({"code": 1, "reason": "정상 처리되었습니다."});
        });
    });
});


router.get("/products", authentication, function(req, res) {
    const page = req.query.page ? parseInt(req.query.page): 0;

    database.connect(function(err) {
        if(err) throw err;

        database.query(sqlmap.adminProductCount,[], function(err, result) {
            if(err) throw err;
            const count = result[0].count;

            database.query(sqlmap.adminProductList,[page * 30], function(err, result) {
                if(err) throw err;

                res.render("admin/products", {'productList': result, "count": count, "page": page});
            });
        });
    });
});

router.get("/products/create", authentication, function(req, res, next) {
    res.render("admin/products-create", {});
});

router.post("/products/create", authentication, upload.array("image"), function(req, res, next) {
    let files = req.files;
    let body = req.body;

    body.is_rent = body.is_rent ? 1 : 0;
    body.is_sell = body.is_sell ? 1 : 0;


    database.connect(function(err) {
        if(err) throw err;

        database.query(sqlmap.insertProduct,
            [body.title, body.contents, body.sell_price, body.rent_price, body.author, body.one_line, body.is_sell ,body.is_rent],
            function(err, result) {
            if(err) throw err;

            for(let i = 0 ; i < files.length ; i++) {
                database.query(sqlmap.insertProductImage,[result.insertId, "/upload/" +files[i].filename, i===0 ? 1 : 0], function(err, result) {
                    if(err) throw err;
                });
            }

            res.redirect(`/admin/products/${result.insertId}`);
        });
    });
});

router.get("/products/:seq", authentication, function(req, res, next) {
    const seq = parseInt(req.params.seq);

    database.connect(function(err) {
        if(err) throw err;

        database.query(sqlmap.productRetrieve,[seq], function(err, result) {
            if(err) throw err;
            if(!result.length) next();

            const product = result[0];

            database.query(sqlmap.productImages, [seq], function(err, result) {
                if(err) throw err;
                const images = result;

                database.query(sqlmap.rentRetrieveByProduct, [seq], function(err, result) {
                    if(err) throw err;
                    const rentList = result;
                    database.query(sqlmap.sellRetrieveByProduct, [seq], function(err, result) {
                        const sellList = result;
                        if(err) throw err;

                        res.render("admin/product-detail", {
                            'product': product,
                            'images': images,
                            'rentList': rentList,
                            'sellList': sellList
                        });
                    });
                });
            });
        });
    });
});


router.post("/products/:seq", authentication, function(req, res, next) {
    const seq = parseInt(req.params.seq);

    database.connect(function(err) {
        if(err) throw err;

        database.query(sqlmap.productRetrieve,[seq], function(err, result) {
            if(err) throw err;
            if(!result.length) next();

            const product = result[0];
            database.query(sqlmap.updateProduct, [req.body.title, req.body.contents, req.body.sell_price, req.body.rent_price, req.body.author, req.body.one_line, seq], function(err, result) {
                if(err) throw err;

                res.redirect(`/admin/products/${seq}`);
            });
        });
    });
});


module.exports = router;