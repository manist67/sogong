var express = require('express');
var router = express.Router();
var sqlmap = require('../db/sqlmap');

/* GET home page. */
router.get('/', function(req, res, next) {
    database.connect(function(err) {
        if(err) throw err;
        database.query(sqlmap.productList, [], function(err, result) {
            if(err) throw err;

            res.render('index', {productList: result});
        });
    });;
});

module.exports = router;
