var createError = require('http-errors');
var express = require('express');
var router = express.Router();
var sqlmap = require('../db/sqlmap');

function isLoggedIn(req, res, next) {
    if(req.session.user) next();
    else res.send({"code": 0, "reason": "not logged in"});
}

router.get('/', function(req, res, next) {
    database.connect(function(err) {
        if(err) throw err;
        database.query(sqlmap.productList, [], function(err, result) {
            if(err) throw err;

            res.render('product', {productList: result});
        });
    });
});

router.get('/:productNo', function(req, res, next) {
    const productNo = req.params.productNo;

    database.connect(function(err) {
        if(err) throw err;
        database.query(sqlmap.productRetrieve, [productNo], function(err, result) {
            if(err) throw err;
            if(result === []) {
                next();
                return;
            }

            const product = result[0];

            database.query(sqlmap.productImages, [productNo], function(err, result) {
                if(err) throw err;

                const images = result;

                database.query(sqlmap.productList, [], function(err, result) {
                    if(err) throw err;

                    res.render('product-detail', {
                        product: product,
                        productImages: images,
                        recommends: result
                    });
                });
            });
        });
    });
});

router.post('/rent/:productNo', isLoggedIn, function(req, res, next) {
    const productNo = req.params.productNo;

    database.connect(function(err) {
        if(err) throw err;
        database.query(sqlmap.productRetrieve, [productNo], function(err, result) {
            if(err) throw err;
            if(result === []) {
                res.send({
                   code: 2,
                   reason: "no product"
                });
                return;
            }

            if(!result[0].is_rent) {
                res.send({
                    code: 3,
                    reason: "cant rent"
                });
                return;
            }

            database.query(sqlmap.rentProduct,
                [req.session.user.seq, productNo, req.body.purpose ,req.body.startDate, req.body.day, req.body.address, req.body.etc],
                function(err, result) {
                if(err) throw err;

                res.send({
                    code: 1,
                    reason: "success"
                });
            });
        });
    });
});


router.post('/sell/:productNo', isLoggedIn, function(req, res, next) {
    const productNo = req.params.productNo;

    database.connect(function(err) {
        if(err) throw err;
        database.query(sqlmap.productRetrieve, [productNo], function(err, result) {
            if(err) throw err;
            if(result === []) {
                res.send({
                    code: 2,
                    reason: "no product"
                });
                return;
            }

            if(!result[0].is_sell) {
                res.send({
                    code: 3,
                    reason: "cant sell"
                });
                return;
            }

            database.query(sqlmap.sellProduct,
                [req.session.user.seq, productNo, req.body.organization, req.body.address, req.body.etc],
                function(err, result) {
                    if(err) throw err;

                    res.send({
                        code: 1,
                        reason: "success"
                    });
                });
        });
    });
});


module.exports = router;