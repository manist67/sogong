var express = require('express');
var crypto = require('crypto');
var nodemailer = require('nodemailer');

var router = express.Router();

var sqlmap = require('../db/sqlmap');

function isLoggedIn(req, res, next) {
    if(req.session.user) next();
    else res.redirect('/');
}

/* GET users listing. */
router.post('/login', function(req, res, next) {
    const id = req.body.id;
    const password = req.body.password;

    database.connect(function(err) {
        if(err) throw err;
        database.query(sqlmap.login, [id, password], function(err, result) {
            if(err) throw err;

            if(!result.length) {
                res.send({
                    code: 0
                });
                return;
            }

            req.session.user = {
                'seq': result[0].seq,
                'id': result[0].id,
                'is_admin': result[0].is_admin
            };

            res.send({
                code: 1
            });
        });
    });

});

router.get('/logout', function(req, res) {
    req.session.user = null;
    res.redirect('/?logout=true');
});

router.get('/signup', function(req, res) {
    res.render('signup')
});

router.post('/signup', function(req, res, next) {
    if(!req.body.id) {
        next();
    }
    if(!req.body.password) {
        next();
    }
    if(!req.body.email) {
        next();
    }

    const password = req.body.password;

    database.connect(function(err) {
        if(err) throw err;

        database.query(sqlmap.idEmailValidation, [req.body.id, req.body.email], function(err, result) {
            if(err) throw err;

            if(result.length !== 0) {
                next();
                return;
            }

            database.query(sqlmap.signup, [req.body.id, password, req.body.email], function(err, result) {
                if(err) throw err;

                res.redirect("/?signup=true");
            });
        });
    });
});

router.get('/validation', function(req, res, next) {
    if(req.query.target === 'id') {
        if(req.query.value.length <= 5) {
            res.send({
                'code': 2
            });
            return;
        }

        database.connect(function(err) {
            if(err) throw err;
            database.query(sqlmap.idValidation, [req.query.value], function(err, result) {
                if(err) throw err;

                if(result.length === 0) {
                    res.send({
                        code: 1
                    });
                } else {
                    res.send({
                        code: 0
                    });
                }
            });
        });
    } else if(req.query.target === 'email') {
        if(!validateEmail(req.query.value)) {
            res.send({
                'code': 2
            });
            return;
        }

        database.connect(function(err) {
            if(err) throw err;
            database.query(sqlmap.emailValidation, [req.query.value], function(err, result) {
                if(err) throw err;

                if(result.length === 0) {
                    res.send({
                        code: 1
                    });
                } else {
                    res.send({
                        code: 0
                    });
                }
            });
        });
    } else {
        next();
    }
});

router.get('/findID', function(req, res, next) {
    res.render("findID", {});
});

router.post('/findID', function(req, res, next) {
    database.connect(function(err) {
        if(err) throw err;
        database.query(sqlmap.emailValidation, [req.body.email], function(err, result) {
            if(err) throw err;

            if(result.length === 0) {
                next();
                return;
            }

            const user = result[0];

            crypto.randomBytes(48, function(err, buffer) {
                const hash = buffer.toString('hex');

                database.query(sqlmap.resetID, [user.seq, hash], function(err, result) {
                    if(err) throw err;
                    const transporter = nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: 'manist9867@gmail.com',
                            pass: 'a@19950417'
                        }
                    });

                    const mailOptions = {
                        from: 'manist9867@gmail.com',
                        to: user.email,
                        subject: '아이디 초기화입니다',
                        text: `<a href="/users/reset/?hash=${hash}">아이디 초기화</a>`
                    };

                    transporter.sendMail(mailOptions, function(error, info){});

                    res.render("findIDSuccess", {
                        "email": req.body.email
                    });
                });
            });
        });
    });
});

router.get('/findPW', function(req, res, next) {
    res.render("findPW", {});
});

router.post('/findPW', function(req, res, next) {
    database.connect(function(err) {
        if(err) throw err;
        database.query(sqlmap.idEmailValidation, [req.body.id, req.body.email], function(err, result) {
            if(err) throw err;

            if(result.length === 0) {
                next();
                return;
            }

            const user = result[0];

            crypto.randomBytes(48, function(err, buffer) {
                const hash = buffer.toString('hex');

                database.query(sqlmap.resetPW, [user.seq, hash], function(err, result) {
                    if(err) throw err;
                    const transporter = nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: 'manist9867@gmail.com',
                            pass: 'a@19950417'
                        }
                    });

                    const mailOptions = {
                        from: 'manist9867@gmail.com',
                        to: user.email,
                        subject: '비밀번호 초기화입니다',
                        text: `<a href="/users/reset/?hash=${hash}">아이디 초기화</a>`
                    };

                    transporter.sendMail(mailOptions, function(error, info){});

                    res.render("findIDSuccess", {
                        "email": req.body.email
                    });
                });
            });
        });
    });
});

router.get('/reset', function(req, res, next) {
    const hash = req.query.hash;
    if(!hash) {
        next();
        return;
    }

    database.connect(function(err) {
        if (err) throw err;
        database.query(sqlmap.resetRetrieve, [hash], function(err, result) {
            if (err) throw err;

            if(result.length === 0) {
                next();
                return;
            }

            if(result[0].type === "id") {
                res.render("getID", {
                    'id': result[0].id
                });
            } else {
                res.render("resetPW", {
                    'hash': hash
                });
            }
        });
    });
});

router.post('/reset', function(req, res, next) {
    const hash = req.query.hash;
    const password = req.body.password;

    if(!hash) {
        next();
        return;
    }

    if(password < 8) {
        next();
        return;
    }

    database.connect(function(err) {
        if (err) throw err;
        database.query(sqlmap.resetRetrieve, [hash], function (err, result) {
            if (err) throw err;
            database.query(sqlmap.passwordUpdate, [password, result[0].seq], function() {
                if (err) throw err;

                res.render("resetPWSuccess", {});
            });
        });
    });
});

router.get('/mypage', isLoggedIn, function(req, res, next) {
    database.connect(function(err) {
        if(err) throw err;

        database.query(sqlmap.userRetrieve, [req.session.user.seq], function(err, result) {
            if(err) throw err;

            res.render("mypage", {
                "userInfo": result[0],
                "message": ""
            });
        });
    });
});

router.post('/mypage', isLoggedIn, function(req, res, next) {
    const email = req.body.email;
    const password = req.body.password;
    let passwordNew = req.body.password_new || "";
    const passwordCheck = req.body.password_new_check || "";

    database.connect(function(err) {
        if(err) throw err;

        database.query(sqlmap.userRetrieve, [req.session.user.seq], function(err, result) {
            if(err) throw err;
            const user = result[0];

            if(password) {
                if(password !== user.password) {
                    res.render("mypage", {
                        "userInfo": user,
                        "message": "기존 비밀번호가 일치하지 않습니다."
                    });
                    return;
                }
                if(passwordNew.length < 8) {
                    res.render("mypage", {
                        "userInfo": user,
                        "message": "비밀번호의 길이는 최소 8자리 이상입니다."
                    });
                    return;
                }
                if(passwordNew !== passwordCheck) {
                    res.render("mypage", {
                        "userInfo": user,
                        "message": "비밀번호가 일치하지 않습니다."
                    });
                    return;
                }
            } else {
                passwordNew = user.password;
            }

            if(!validateEmail(email)) {
                res.render("mypage", {
                    "userInfo": user,
                    "message": "이메일이 형식에 맞지않습니다."
                });
                return;
            }

            database.query(sqlmap.emailValidation, [email], function(err, result) {
                if(err) throw err;

                if(user.email !== email && result.length !== 0) {
                    res.render("mypage", {
                        "userInfo": user,
                        "message": "중복된 이메일입니다."
                    });
                    return;
                }

                database.query(sqlmap.userUpdate, [passwordNew, email, user.seq], function(err, result) {
                    if(err) throw err;
                    user.email = email;

                    res.render("mypage", {
                        "userInfo": user,
                        "message": "수정되었습니다."
                    });
                });
            });
        });
    });
});

router.get('/myrent', isLoggedIn, function(req, res, next) {
    database.connect(function(err) {
        if(err) throw err;

        database.query(sqlmap.rentRetrieveByUser, [req.session.user.seq], function(err, result) {
            if(err) throw err;

            res.render("myrent", {
                "rentList": result
            });
        });
    });
});

router.get('/mysell', isLoggedIn, function(req, res, next) {
    database.connect(function(err) {
        if(err) throw err;

        database.query(sqlmap.sellRetrieveByUser, [req.session.user.seq], function(err, result) {
            if(err) throw err;

            console.log(result[0]);

            res.render("mysell", {
                "sellList": result
            });
        });
    });
});
module.exports = router;


function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}